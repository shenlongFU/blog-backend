<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */

class PostsController extends AppController {
	public static $limit = '7';

    public $helpers = array('Html', 'Form');
    public function index() {
        $this->paginate = array(
            'limit' => 3,
            'order' => array('id' => 'asc'),
        );
        $this->set('posts', $this->paginate('Post'));
    }
    public function view($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Post->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }
        $this->set('post', $post);
    }
    public function show($page='1'){
        $this->render(false);
        // $this->layout = false;
        //set default response
        $response = array('status'=>'failed', 'message'=>'Failed to process request');
		/*SELECT column FROM table
LIMIT 10 OFFSET 10;*/
		  $total = $this->Post->find('count');
		  $result = $this->Post->find('all', array(
			'limit' => 7 ,
			'offset' =>(($page-1)*7)

		  ));
            if(!empty($result)){
                $response = array('status'=>'success','total'=>$total,'data'=>$result);
            } else {
                $response['message'] = 'Found no matching data';
            }

        $this->response->header('Access-Control-Allow-Origin','*');
        $this->response->type('application/json;charset=utf-8');
        $this->response->body(json_encode($response));
        //return $this->response->send();
    }
	public function search($page='1',$title=''){
		$this->render(false);
		// $this->layout = false;
		//set default response
		$response = array('status'=>'failed', 'message'=>'Failed to process request');
        //$title = trim($title);
		$result = $this->Post->find('all', array(
			'limit' => 7,
			'offset' =>(($page-1)*7),
	         'conditions' => array(
					'title LIKE' => '%'.$title.'%')

		));
		$total = $this->Post->find('count', array(
			'conditions' => array(
				'title LIKE' => '%'.$title.'%')

		));
		/*
		$result = $this->Post->find('all', array(
			'conditions' => array(
					'title LIKE' => '%'.$title.'%',
			)
			));*/
		if(!empty($result)){
			$response = array('status'=>'success','total'=>$total,'data'=>$result);
		} else {
			$response['message'] = 'Found no matching data';
		}

		$this->response->header('Access-Control-Allow-Origin','*');
		$this->response->type('application/json;charset=utf-8');
		$this->response->body(json_encode($response));
		//return $this->response->send();
	}
    public function detail($id = null){
        $this->render(false);
       // $this->layout = false;
        //set default response
        $response = array('status'=>'failed', 'message'=>'Failed to process request');

        //check if ID was passed
        if(!empty($id)){

            //find data by ID
            $result = $this->Post->findById($id);
            if(!empty($result)){
                $response = array('status'=>'success','data'=>$result);
            } else {
                $response['message'] = 'Found no matching data';
            }
        } else {
            $response['message'] = "Please provide ID";
        }
		$this->response->header('Access-Control-Allow-Origin','*');
        $this->response->type('application/json');
        $this->response->body(json_encode($response));
        //return $this->response->send();
    }
    public function update(){
        //set layout as false to unset default CakePHP layout. This is to prevent our JSON response from mixing with HTML
        $this->render(false);
        //set default response
        $response = array('status'=>'failed', 'message'=>'HTTP method not allowed');

        //check if HTTP method is PUT
        if($this->request->is('POST')){
            //get data from request object
            $data = $this->request->input('json_decode', true);
            if(empty($data)){
                $data = $this->request->data;
            }

            //check if product ID was provided
            if(!empty($data['id'])){

                //set the product ID to update
                $this->Post->id = $data['id'];
                if($this->Post->save($data)){
                    $response = array('status'=>'success','message'=>'Post successfully updated');
                } else {
                    $response['message'] = "Failed to update post";
                }
            } else {
                $response['message'] = "Please provide post's ID";
            }
        }
		$this->response->header('Access-Control-Allow-Origin','*');
        $this->response->type('application/json');
        $this->response->body(json_encode($response));
       // return $this->response->send();
    }
    public function edit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        $post = $this->Post->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $this->Post->id = $id;
            if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Your post has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to update your post.'));
        }

        if (!$this->request->data) {
            $this->request->data = $post;
        }
    }
    public function add() {
        if ($this->request->is('post')) {
            $this->Post->create();
            if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Your post has been saved.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to add your post.'));
        }
    }
    public function store(){
        $this->render(false);
        $response = array('status'=>'failed', 'message'=>'HTTP method not allowed');
        if($this->request->is('post')){

            //get data from request object
            $data = $this->request->input('json_decode', true);
            if(empty($data)){
                $data = $this->request->data;
            }

            //response if post data or form data was not passed
            $response = array('status'=>'failed', 'message'=>'Please provide form data');

            if(!empty($data)){
                //call the model's save function
                if($this->Post->save($data)){
                    //return success
                    $response = array('status'=>'success','message'=>'Product successfully created');
                } else{
                    $response = array('status'=>'failed', 'message'=>'Failed to save data');
                }
            }
        }
		$this->response->header('Access-Control-Allow-Origin','*');
        $this->response->type('application/json');
        $this->response->body(json_encode($response));
       // return $this->response->send();
    }
    public function remove(){
        $this->render(false);

        //set default response
        $response = array('status'=>'failed', 'message'=>'HTTP method not allowed');

        //check if HTTP method is DELETE
        if($this->request->is('post')){
            //get data from request object
			$response = array('status'=>'success','message'=>'Posts not deleted');
            $data = $this->request->input('json_decode', true);
            if(empty($data)){

                $data = $this->request->data;
				$response = array('status'=>'success','message'=>$this->request);
            }

            //check if product ID was provided
            if(!empty($data['id'])){
                $id=$data['id'];
				$response = array('status'=>'success','message'=>'Posts 2');
                if($this->Post->delete($id, true)){
                    $response = array('status'=>'success','message'=>'Posts successfully deleted');
                }else{
                    $response = array('message'=>'Posts id not found!');
                }
            }
        }
		$this->response->header('Access-Control-Allow-Origin','*');
        $this->response->type('application/json');
        $this->response->body(json_encode($response));
        //return $this->response->send();
    }
    public function delete($id) {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        if ($this->Post->delete($id)) {
            $this->Flash->success(
                __('The post with id: %s has been deleted.', h($id))
            );
        } else {
            $this->Flash->error(
                __('The post with id: %s could not be deleted.', h($id))
            );
        }

        return $this->redirect(array('action' => 'index'));
    }

}
