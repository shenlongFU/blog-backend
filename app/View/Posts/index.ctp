<h1>Blog posts</h1>
<button>
    <?php echo $this->Html->link(
        'Add Post',
        array('controller' => 'posts', 'action' => 'add')
    ); ?>
</button>
<table>
    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Created</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($posts as $post): ?>
        <tr>
            <td><?php echo $post['Post']['id']; ?></td>
            <td>
                <?php echo $this->Html->link($post['Post']['title'],
                    array('controller' => 'posts', 'action' => 'view', $post['Post']['id'])); ?>
            </td>

            <td><?php echo $post['Post']['created']; ?></td>
            <td>
                <?php
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $post['Post']['id']),
                    array('confirm' => 'Are you sure?')
                );
                ?>
                <?php
                echo $this->Html->link(
                    'Edit', array('action' => 'edit', $post['Post']['id'])
                );
                ?>
            </td>
        </tr>

    <?php endforeach; ?>
    <?php unset($post); ?>


</table>
<?php
//echo $this->Paginator->prev('« Previous ', null, null, array('class' => 'disabled')); //Shows the next and previous links
echo " | ".$this->Paginator->numbers()." | "; //Shows the page numbers
//echo $this->Paginator->next(' Next »', null, null, array('class' => 'disabled')); //Shows the next and previous links
//echo " Page ".$this->Paginator->counter(); // prints X of Y, where X is current page and Y is number of pages
?>